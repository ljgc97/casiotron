#include "app_clock.h"
#include "app_bsp.h"

#define CLK_IDLE 0
#define CLK_ALARM 1
#define CLK_SET_CONFIG 2
#define CLK_ALARM_DETAILS 3
#define CLK_PRINT_TIME 4

void RTC_TimeConfig(void);
void RTC_DateConfig(void);
void RTC_AlarmConfig(void);
void startAlarm(void);
void getAlarmDetails(void);
void printTimeDate(void);
void setConfig(void);

RTC_HandleTypeDef RtcHandle;
RTC_TimeTypeDef RTC_TimeInit;
RTC_DateTypeDef RTC_DateInit;
RTC_AlarmTypeDef RTC_AlarmInit;
RTC_TimeTypeDef RTC_TimeRead;
RTC_DateTypeDef RTC_DateRead;
RTC_AlarmTypeDef RTC_AlarmRead;

extern SERIAL_MsgTypeDef serialStruct;
extern void initialise_monitor_handles(void);

__IO ITStatus alarmStat = RESET;
uint8_t printFlag = 0, buttonFlag = 0, alarmFlag = 0;

void clock_init(void)
{
    //inicializa el debug openocd
    initialise_monitor_handles();
    printf("\n");

    //Inicializar RTC
    RtcHandle.Instance = RTC;
    RtcHandle.Init.HourFormat = RTC_HOURFORMAT_24;
    RtcHandle.Init.AsynchPrediv = 0x7F;
    RtcHandle.Init.SynchPrediv = 0xFF;
    RtcHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
    RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_LOW;
    RtcHandle.Init.OutPutType = RTC_OUTPUT_TYPE_PUSHPULL;
    HAL_RTC_Init(&RtcHandle);

    //Inicializar GPIO UserButton
    __HAL_RCC_GPIOC_CLK_ENABLE();
    GPIO_InitTypeDef UserButton;

    UserButton.Mode = GPIO_MODE_IT_FALLING;
    UserButton.Pull = GPIO_NOPULL;
    UserButton.Speed = GPIO_SPEED_FREQ_LOW;
    UserButton.Pin = GPIO_PIN_13;
    HAL_GPIO_Init(GPIOC, &UserButton);

    HAL_NVIC_SetPriority(EXTI4_15_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
    
    //Configuracion inicial RTC
    RTC_TimeInit.Hours = 0;
    RTC_TimeInit.Minutes = 0;
    RTC_TimeInit.Seconds = 0;
    HAL_RTC_SetTime(&RtcHandle, &RTC_TimeInit, RTC_FORMAT_BIN);

    RTC_DateInit.Date = 1;
    RTC_DateInit.Month = RTC_MONTH_JANUARY;
    RTC_DateInit.Year = 0;
    HAL_RTC_SetDate(&RtcHandle, &RTC_DateInit, RTC_FORMAT_BIN);
}

void RTC_TimeConfig(void)
{
    RTC_TimeInit.Hours = serialStruct.param1;
    RTC_TimeInit.Minutes = serialStruct.param2;
    RTC_TimeInit.Seconds = serialStruct.param3;
    HAL_RTC_SetTime(&RtcHandle, &RTC_TimeInit, RTC_FORMAT_BIN);
}

void RTC_DateConfig(void)
{
    RTC_DateInit.Date = serialStruct.param1;
    RTC_DateInit.Month = serialStruct.param2;
    RTC_DateInit.Year = serialStruct.param3;
    HAL_RTC_SetDate(&RtcHandle, &RTC_DateInit, RTC_FORMAT_BIN);
}

void RTC_AlarmConfig(void)
{
    memset(&RTC_AlarmInit, 0, sizeof(RTC_AlarmInit));

    HAL_RTC_DeactivateAlarm(&RtcHandle, RTC_ALARM_A);

    RTC_AlarmInit.Alarm = RTC_ALARM_A;
    RTC_AlarmInit.AlarmTime.Hours = serialStruct.param1;
    RTC_AlarmInit.AlarmTime.Minutes = serialStruct.param2;
    RTC_AlarmInit.AlarmMask = RTC_ALARMMASK_DATEWEEKDAY | RTC_ALARMMASK_SECONDS;
    RTC_AlarmInit.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_NONE;
    HAL_RTC_SetAlarm_IT(&RtcHandle, &RTC_AlarmInit, RTC_FORMAT_BIN);
}

void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
    alarmStat = SET;
    HAL_RTC_DeactivateAlarm(&RtcHandle, RTC_ALARM_A);
}

void setConfig(void)
{
    switch (serialStruct.msg)
    {
    case TIME:
        RTC_TimeConfig();
        break;
    case DATE:
        RTC_DateConfig();
        break;
    case ALARM:
        RTC_AlarmConfig(); 
        break;

    default:
        serialStruct.msg = 0;
        break;
    }
    serialStruct.msg = NONE;
}

void printTimeDate(void)
{
    static uint8_t i = 0;
    static char a[32]={0};
    HAL_RTC_GetTime(&RtcHandle, &RTC_TimeRead, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&RtcHandle, &RTC_DateRead, RTC_FORMAT_BIN);
    if(i==0){
        sprintf( a, "Time: %02d:%02d:%02d - Date: %02d:%02d:%02d", RTC_TimeRead.Hours, RTC_TimeRead.Minutes, RTC_TimeRead.Seconds, RTC_DateRead.Date, RTC_DateRead.Month, RTC_DateRead.Year);
    }
    printf("%c", a[i]);
    i++;
    if(a[i]=='\0'){
        printf("\n");
        i=0;
        memset(a, 0, sizeof(a));
        printFlag = 0;
    }   
}

void startAlarm(void)
{
    static uint8_t time = 0;
    static uint8_t i = 0;
    char a[]="ALERT ALARM!";

    printf("%c", a[i]);
    i++;

    if(a[i]=='\0'){
        printf("\n");
        i=0;
        memset(a, 0, sizeof(a));
        alarmFlag = 0;
        time++;
    }    
    if(time>59){
        alarmStat = RESET;
        time = 0;
    }
}

void getAlarmDetails(void)
{ 
    static uint8_t i = 0;
    static char a[17]={0};
    HAL_RTC_GetAlarm(&RtcHandle, &RTC_AlarmRead, RTC_ALARM_A, RTC_FORMAT_BIN);

    if(i==0){
        sprintf( a, "A %02d:%02d:00", RTC_AlarmRead.AlarmTime.Hours, RTC_AlarmRead.AlarmTime.Minutes);
    }
    printf("%c", a[i]);
    i++;
    if(a[i]=='\0'){
        printf("\n");
        i=0;
        memset(a, 0, sizeof(a));
        buttonFlag = 0;
    }
}

void clock_task(void)
{
    static uint8_t clockState = CLK_IDLE;
    static int8_t lastSec1, lastSec2, lastSec3;
    
    HAL_RTC_GetTime(&RtcHandle, &RTC_TimeRead, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&RtcHandle, &RTC_DateRead, RTC_FORMAT_BIN);
    switch (clockState)
    {
    case CLK_IDLE:
        if ((RTC_TimeRead.Seconds - lastSec1) != 0)
        {
            clockState = CLK_PRINT_TIME;
            printFlag = 1;
            HAL_RTC_GetTime(&RtcHandle, &RTC_TimeRead, RTC_FORMAT_BIN);
            HAL_RTC_GetDate(&RtcHandle, &RTC_DateRead, RTC_FORMAT_BIN);
            lastSec1 = RTC_TimeRead.Seconds;
        }
        if(printFlag == 1){
            clockState = CLK_PRINT_TIME;
        }
        if (alarmStat == SET)
        {
            if ((RTC_TimeRead.Seconds - lastSec2) != 0)
            {
                clockState = CLK_ALARM;
                alarmFlag = 1;
                printFlag = 0;
                buttonFlag = 0;
                HAL_RTC_GetTime(&RtcHandle, &RTC_TimeRead, RTC_FORMAT_BIN);
                HAL_RTC_GetDate(&RtcHandle, &RTC_DateRead, RTC_FORMAT_BIN);
                lastSec2 = RTC_TimeRead.Seconds;
            }
        }
        if(alarmFlag == 1){
            clockState = CLK_ALARM;
        }
        if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13) == 0)
        {
            if ((RTC_TimeRead.Seconds - lastSec3) != 0)
            {
                clockState = CLK_ALARM_DETAILS;
                buttonFlag = 1;
                printFlag = 0;
                alarmFlag = 0;
                HAL_RTC_GetTime(&RtcHandle, &RTC_TimeRead, RTC_FORMAT_BIN);
                HAL_RTC_GetDate(&RtcHandle, &RTC_DateRead, RTC_FORMAT_BIN);
                lastSec3 = RTC_TimeRead.Seconds;
            }
            alarmStat = RESET;
        }
        if(buttonFlag == 1){
            clockState = CLK_ALARM_DETAILS;
        }
        if (serialStruct.msg != NONE)
        {
            clockState = CLK_SET_CONFIG;
        }
        break;
    case CLK_ALARM:
        startAlarm();
        clockState = CLK_IDLE;
        break;
    case CLK_SET_CONFIG:
        setConfig();
        clockState = CLK_IDLE;
        break;
    case CLK_ALARM_DETAILS:
        getAlarmDetails();
        clockState = CLK_IDLE;
        break;
    case CLK_PRINT_TIME:
        printTimeDate();
        clockState = CLK_IDLE;
        break;
    
    default:
        clockState = CLK_IDLE;
        break;
    }
}
