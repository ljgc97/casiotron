#include "app_bsp.h"
#include "app_serial.h"
#include "app_clock.h"


/**------------------------------------------------------------------------------------------------
Brief.- Punto de entrada del programa
-------------------------------------------------------------------------------------------------*/
void heart_init(void);
void heart_beat(void);
void dog_init(void);
void pet_the_dog(void);

WWDG_HandleTypeDef   WwdgHandle;

int main(void)
{

    HAL_Init();
    serial_init();
    clock_init();
    heart_init();
    dog_init();

    for (;;)
    {
        serial_task();
        clock_task();
        heart_beat();
        pet_the_dog();
    }
    return 0u;
}

void heart_init(void)
{
    __HAL_RCC_GPIOA_CLK_ENABLE();
    GPIO_InitTypeDef UserLed;
    UserLed.Mode = GPIO_MODE_OUTPUT_PP;
    UserLed.Pull = GPIO_NOPULL;
    UserLed.Speed = GPIO_SPEED_FREQ_HIGH;
    UserLed.Pin = GPIO_PIN_5 | GPIO_PIN_6;
    HAL_GPIO_Init(GPIOA, &UserLed);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5 | GPIO_PIN_6, RESET);
}

void heart_beat(void)
{
    static uint32_t heartTick = 0;

    if(heartTick == 0){
        heartTick = HAL_GetTick();
    }
    if ((HAL_GetTick()-heartTick) >= 300)
    {
        HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_5);
        heartTick = HAL_GetTick();
    }
}

void dog_init(void)
{
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != RESET)
    {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, SET);
        HAL_Delay(4000);
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, RESET);
    }
    __HAL_RCC_CLEAR_RESET_FLAGS();

    WwdgHandle.Instance = WWDG;
    WwdgHandle.Init.Prescaler = WWDG_PRESCALER_8;
    WwdgHandle.Init.Window = 110;
    WwdgHandle.Init.Counter = 127;
    WwdgHandle.Init.EWIMode = WWDG_EWI_DISABLE;
    HAL_WWDG_Init(&WwdgHandle);
}

void pet_the_dog(void)
{
    static uint32_t dogTick = 0;

    if(dogTick == 0){
        dogTick = HAL_GetTick();
    }
    if ((HAL_GetTick()-dogTick) >= 70)
    {
        HAL_WWDG_Refresh(&WwdgHandle);
        dogTick = HAL_GetTick();
    }
}








