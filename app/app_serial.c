#include "app_serial.h"
#include "app_bsp.h"

#define SERIAL_IDLE 0
#define SERIAL_SPLIT_AT 1
#define SERIAL_ATOI 2
#define SERIAL_ASSIGN_MSG 3
#define SERIAL_ERROR 4

void splitCommand(void);
uint8_t convertCommands(void);
uint8_t assignTypeMsg(void);
void clearBuffer(void);

uint8_t RxBuffer[20];
uint8_t tempBuffer[20];
uint8_t RxByte;
uint8_t *at = NULL;
uint8_t *com = NULL;
uint8_t *c1 = NULL;
uint8_t *c2 = NULL;
uint8_t *c3 = NULL;

UART_HandleTypeDef UartHandle;
SERIAL_MsgTypeDef serialStruct;

__IO ITStatus uartStat = RESET;

void serial_task(void){
    static uint8_t serialState = SERIAL_IDLE;

    switch (serialState)
    {
    case SERIAL_IDLE:
        if (uartStat == SET)
        {
            uartStat = RESET;
            serialState = SERIAL_SPLIT_AT;
        }
        break;
    case SERIAL_SPLIT_AT:
        splitCommand();
        serialState = SERIAL_ATOI;
        break;
    case SERIAL_ATOI:
        if (convertCommands() == 1)
        {
            serialState = SERIAL_ERROR;
        }
        else
        {
            serialState = SERIAL_ASSIGN_MSG;
        }
        break;
    case SERIAL_ASSIGN_MSG:
        if (assignTypeMsg() == 1)
        {
            serialState = SERIAL_ERROR;
        }
        else
        {
            clearBuffer();
            serialState = SERIAL_IDLE;
        }
        break;
    case SERIAL_ERROR:
        clearBuffer();
        HAL_UART_Transmit(&UartHandle, (uint8_t *)"\r\nERROR\r\n", 9, 5000);
        serialState = SERIAL_IDLE;
    default:
        serialState = SERIAL_IDLE;
        break;
    }
}

void serial_init(void)
{
    UartHandle.Instance = USART2;
    UartHandle.Init.BaudRate = 9600;
    UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
    UartHandle.Init.StopBits = UART_STOPBITS_1;
    UartHandle.Init.Parity = UART_PARITY_NONE;
    UartHandle.Init.Mode = UART_MODE_TX_RX;
    UartHandle.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    UartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
    HAL_UART_Init(&UartHandle);

     HAL_UART_Receive_IT(&UartHandle, &RxByte, 1);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    static uint32_t i = 0;
    RxBuffer[i] = RxByte;
    i++;
    if (RxBuffer[i - 1] == '\r')
    {
        uartStat = SET;
        i = 0;
    }
    HAL_UART_Receive_IT(&UartHandle, &RxByte, 1);
}

void splitCommand(void)
{
    strcpy((char *)tempBuffer, (char *)RxBuffer);
    at = (uint8_t *)strtok((char *)tempBuffer, "+");
    com = (uint8_t *)strtok(NULL, "=");
    c1 = (uint8_t *)strtok(NULL, ",");
    c2 = (uint8_t *)strtok(NULL, ",");
    c3 = (uint8_t *)strtok(NULL, "\r");
}

uint8_t convertCommands(void)
{
    uint8_t *cadena[] = {c1, c2, c3};
    for (uint8_t i = 0; i < 3; i++)
    {
        for (uint8_t j = 0; cadena[i][j] != '\0'; j++)
        {
            if (cadena[i][j] >= '0' && cadena[i][j] <= '9')
            {
                //Nothing
            }
            else
            {
                return 1; //error, datos no numericos
            }
        }
    }
    serialStruct.param1 = atoi((char *)c1);
    serialStruct.param2 = atoi((char *)c2);
    serialStruct.param3 = atoi((char *)c3);
    return 0;
}

uint8_t assignTypeMsg(void)
{
    uint8_t err = 0;

    if (strcmp((char *)at, "AT") == 0)
    {
        if (strcmp((char *)com, "TIME") == 0)
        {
            if (serialStruct.param1 < 0 || serialStruct.param1 > 23)
            {
                err = 1;
            }
            if (serialStruct.param2 < 0 || serialStruct.param2 > 59)
            {
                err = 1;
            }
            if (serialStruct.param3 < 0 || serialStruct.param3 > 59)
            {
                err = 1;
            }
            if (err == 0)
            {
                serialStruct.msg = TIME;
            }
        }
        else if (strcmp((char *)com, "DATE") == 0)
        {
            if (serialStruct.param1 < 1 || serialStruct.param1 > 31)
            {
                err = 1;
            }
            if (serialStruct.param2 < 1 || serialStruct.param2 > 12)
            {
                err = 1;
            }
            if (serialStruct.param3 < 0 || serialStruct.param3 > 99)
            {
                err = 1;
            }
            if (err == 0)
            {
                serialStruct.msg = DATE;
            }
        }
        else if (strcmp((char *)com, "ALARM") == 0)
        {
            if (serialStruct.param1 < 0 || serialStruct.param1 > 23)
            {
                err = 1;
            }
            if (serialStruct.param2 < 0 || serialStruct.param2 > 59)
            {
                err = 1;
            }
            if (err == 0)
            {
                serialStruct.msg = ALARM;
            }
        }
        else
        {
            err = 1;
        }
    }
    else
    {
        err = 1;
    }
    return err;
}

void clearBuffer(void){
    memset(RxBuffer, 0, sizeof(RxBuffer));
    memset(tempBuffer, 0, sizeof(tempBuffer));
}


