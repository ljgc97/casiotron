#ifndef _BSP_H_
#define _BSP_H_

#define NONE 0
#define TIME 1
#define DATE 2 
#define ALARM 3

#include "stm32f0xx.h"
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	uint8_t msg;	 // tipo de mensaje
	uint8_t param1;	 // hora o dia
    uint8_t param2;	 // minutos o mes
    uint16_t param3; // segundos o año
}SERIAL_MsgTypeDef;

#endif

